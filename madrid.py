#!/usr/bin/env python3
# Copyright (c) 2022 Lukas Fink
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import re
import random
from urllib.parse import urljoin

BASE_URL = 'https://madrid.maschinenbaufest.de/'

INGR_ALC = {'bluecuracao', 'gin', 'vodka', 'we_rum'}
INGR_JUICE = {'ananas', 'bananensaft', 'kirschsaft', 'maracuja', 'osaft'}
INGR_SOFT = {'bitterlemon', 'cola', 'ginger', 'tonic'}
INGR_SIRUP = {'grenadine', 'kokossirup'}
INGR_EMPTY = {'sprudel', 'wasserStill', 'luft'}
INGR_NONALC = INGR_JUICE | INGR_SOFT | INGR_SIRUP | {'zitrone'}
INGR_ALL = INGR_ALC | INGR_NONALC | INGR_EMPTY

ALC_MAX = 0.3

#EMPTY_INGR = {
#    'ananas': 0.,
#    'bananensaft': 0.,
#    'kirschsaft': 0.,
#    'maracuja': 0.,
#    'osaft': 0.,
#    'zitrone': 0.,
#    'bitterlemon': 0.,
#    'cola': 0.,
#    'ginger': 0.,
#    'sprudel': 0.,
#    'tonic': 0.,
#    'wasserStill': 0.,
#    'bluecuracao': 0.,
#    'gin': 0.,
#    'vodka': 0.,
#    'we_rum': 0.,
#    'grenadine': 0.,
#    'kokossirup': 0.,
#    'luft': 0.
#}

WULU_INGR = {'ananas': 0.7, 'gin': 0.3}

#def gen_equality(alc_ratio=ALC_MAX):
#    return {name: alc_ratio / len(INGR_ALC) for name in INGR_ALC} | \
#           {name: (1. - alc_ratio) / len(INGR_NONALC) for name in INGR_NONALC}
#
#EQUALITY_INGR = gen_equality()

class Error(Exception):
    pass

class InvalidIngredientsError(Error):
    def __init__(self, ingr, message=None):
        if message == None:
            super().__init__('Oli sagt nein.')
        else:
            super().__init__(message)

        self.ingr = ingr

def register(ingr):
    data = {f'ingredient[{name}]': val for name, val in ingr.items() if val > 0.} | {'action': 'save'}
    r = requests.post(urljoin(BASE_URL, 'wizard.php'), data=data)
    if 'Ungültige Drinkdaten.' in r.text:
        raise InvalidIngredientsError(ingr)
    return re.search(r'''<div\s*class=['"]drink-code['"]>\s*(\w+)\s*</div>''', r.text).group(1)

def scale_ingr(ingr, factor):
    return {name: val * factor for name, val in ingr.items()}

def rdg(rng, alc_ratio=ALC_MAX, chaos=True, chaos_flags=()):
    if chaos and rng.randrange(100) == 0:
        return chaos_rdg(rng, alc_ratio, chaos_flags)

    modes = {'juice', 'soft'}
    mode = rng.choice(tuple(modes))

    if mode == 'juice':
        nalc_ingr = juice_rdg(rng)
    elif mode == 'soft':
        soft = rng.choice(tuple(INGR_SOFT))
        if rng.randrange(4) == 0:
            juice_ratio = rng.triangular(0., 0.4)
            nalc_ingr = scale_ingr(juice_rdg(rng), juice_ratio) | {soft: 1. - juice_ratio}
        else:
            nalc_ingr = {soft: 1.}

    sirup = rng.choice(tuple(INGR_SIRUP))
    sirup_ratio = rng.triangular(0., 0.2) if rng.randrange(6) == 0 else 0.
    lemon_ratio = rng.triangular(0., 0.2) if rng.randrange(4) == 0 else 0.
    nalc_ingr = {sirup: sirup_ratio, 'zitrone': lemon_ratio} | scale_ingr(nalc_ingr, 1. - sirup_ratio - lemon_ratio)

    alc = rng.choice(tuple(INGR_ALC))
    curacao_ratio = alc_ratio * rng.triangular(0., 0.4) if rng.randrange(4) == 0 and alc != 'bluecuracao' else 0.
    return {'bluecuracao': curacao_ratio, alc: alc_ratio - curacao_ratio} | scale_ingr(nalc_ingr, 1. - alc_ratio)

def chaos_rdg(rng, alc_ratio=ALC_MAX, flags=()):
    modes = {'single', 'juice', 'schorle', 'sirup_schorle', 'shot', 'watered_down', 'equality', 'sour'}
    if alc_ratio < 0.05:
        modes -= {'watered_down', 'shot'}
    if 'force_alc' in flags:
        modes -= {'single', 'juice', 'schorle', 'sirup_schorle'}
    mode = rng.choice(tuple(modes))

    if mode == 'single':
        return {rng.choice(tuple(INGR_NONALC | (set() if 'force_content' in flags else INGR_EMPTY))): 1.}
    elif mode == 'juice':
        return juice_rdg(rng)
    elif mode == 'schorle':
        return scale_ingr(juice_rdg(rng), 0.6) | {'sprudel': 0.4}
    elif mode == 'sirup_schorle':
        return {rng.choice(tuple(INGR_SIRUP)): 0.2, 'sprudel': 0.8}
    elif mode == 'shot':
        return {rng.choice(tuple(INGR_ALC)): ALC_MAX, 'luft': 1. - ALC_MAX}
    elif mode == 'watered_down':
        return {rng.choice(tuple(INGR_ALC)): alc_ratio, rng.choice(tuple({'sprudel', 'wasserStill'})): 1. - alc_ratio}
#    elif mode == 'equality': # Oli ist gemein!
#        return gen_equality(alc_ratio)
    elif mode == 'sour':
        return {rng.choice(tuple(INGR_ALC)): alc_ratio, 'zitrone': 1. - alc_ratio}

def juice_rdg(rng):
    remaining = 1.
    ingr = {}
    for i in range(int(rng.triangular(0., 4., 0.))):
        juice = rng.choice(tuple(INGR_JUICE - set(ingr.keys())))
        ratio = rng.triangular(0., 1., 0.6) * remaining
        ingr |= {juice: ratio}
        remaining -= ratio
    ingr |= {rng.choice(tuple(INGR_JUICE - set(ingr.keys()))): remaining}
    return ingr

if __name__ == '__main__':
    print(register(rdg(random.SystemRandom())))
